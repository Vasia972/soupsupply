FROM public.ecr.aws/lambda/python:3.9

ENV POETRY_VERSION 1.1.8

RUN pip install poetry==$POETRY_VERSION
COPY poetry.lock pyproject.toml ./
RUN poetry export -f requirements.txt --without-hashes > requirements.txt

RUN pip3 install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

COPY . ${LAMBDA_TASK_ROOT}

CMD [ "soupsupply/lambdas.tg_controller_lambda.tg_controller_lambda" ]
