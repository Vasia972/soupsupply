import sys
from typing import Any

from dependency_injector.wiring import Provide, inject

from soupsupply.containers import Container, build_container


@inject
def create_users_tb(dynamodb_client: Any = Provide[Container.dynamodb_client]):
    try:
        dynamodb_client.create_table(
            TableName='SoupUsers',
            ProvisionedThroughput={
                'ReadCapacityUnits': 4,
                'WriteCapacityUnits': 1,
            },
            KeySchema=[
                {
                    'AttributeName': 'user_id',
                    'KeyType': 'HASH',
                },
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'user_id',
                    'AttributeType': 'N',
                },
            ],
        )
    except dynamodb_client.exceptions.ResourceInUseException:
        return


@inject
def drop_users_tb(dynamodb_client: Any = Provide[Container.dynamodb_client]):
    try:
        dynamodb_client.delete_table(TableName='SoupUsers')
    except dynamodb_client.exceptions.ResourceNotFoundException:
        return


if __name__ == '__main__':
    container = build_container()
    container.wire(modules=[sys.modules[__name__]])

    create_users_tb()
