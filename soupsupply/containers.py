import os

import boto3
from dependency_injector import containers, providers

from soupsupply.repository.dynamodb import DynamodbUserRepository
from soupsupply.soup_api import SoupAPI
from soupsupply.soup_service.service import SoupService
from soupsupply.tg.api import TgAPI
from soupsupply.tg.service import TgSoupService


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()

    aws_session: boto3.session.Session = providers.Resource(
        boto3.session.Session,
        aws_session_token=config.aws_session_token,
        aws_access_key_id=config.aws_access_key_id,
        aws_secret_access_key=config.aws_secret_access_key,
    )

    dynamodb_client: boto3.session.Session.client = providers.Resource(
        aws_session.provided.client.call(),  # type:ignore
        service_name='dynamodb',
        endpoint_url=config.dynamo_endpoint,
        region_name=config.aws_region,
    )

    dynamodb: boto3.session.Session.resource = providers.Resource(
        aws_session.provided.resource.call(),  # type:ignore
        service_name='dynamodb',
        endpoint_url=config.dynamo_endpoint,
        region_name=config.aws_region,
    )

    user_repository: DynamodbUserRepository = providers.Factory(
        DynamodbUserRepository,
        dynamodb=dynamodb,
    )

    soup_api: SoupAPI = providers.Singleton(SoupAPI)

    soup_service: SoupService = providers.Factory(
        SoupService,
        user_repo=user_repository,
        soup_api=soup_api,
    )

    tg_api: TgAPI = providers.Singleton(
        TgAPI,
        bot_token=config.telegram_bot_token,
    )

    tg_service: TgSoupService = providers.Factory(
        TgSoupService,
        tg_api=tg_api,
        soup_service=soup_service,
    )


def build_container() -> Container:
    print('Environment:', os.environ)
    container = Container()
    container.config.dynamo_endpoint.from_env('AWS_DYNAMO_ENDPOINT')
    container.config.aws_region.from_env('AWS_REGION')
    container.config.aws_session_token.from_env('AWS_SESSION_TOKEN')
    container.config.aws_access_key_id.from_env('AWS_ACCESS_KEY_ID')
    container.config.aws_secret_access_key.from_env('AWS_SECRET_ACCESS_KEY')
    container.config.telegram_bot_token.from_env('TELEGRAM_BOT_TOKEN')

    container.init_resources()

    return container
