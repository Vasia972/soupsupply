import enum
from typing import Optional

from pydantic import BaseModel


class UserState(enum.Enum):
    NEW = 'new'
    ON_CARD_INPUT = 'on_card_input'
    READY = 'ready'


class User(BaseModel):
    user_id: int
    card: Optional[str]
    state: UserState = UserState.NEW
