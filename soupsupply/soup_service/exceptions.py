class UserNotFound(Exception):
    pass


class UserCardIsMissing(Exception):
    pass


class CardValidationError(Exception):
    pass


class CardNumberIsTooShort(CardValidationError):
    pass


class CardNumberIsTooLong(CardValidationError):
    pass


class CardIsNotNumeric(CardValidationError):
    pass
