from typing import Optional

from soupsupply.repository.dynamodb import DynamodbUserRepository
from soupsupply.soup_api import SoupAPI
from soupsupply.soup_api.data import CardData
from soupsupply.soup_service.exceptions import (CardIsNotNumeric,
                                                CardNumberIsTooLong,
                                                CardNumberIsTooShort,
                                                UserCardIsMissing,
                                                UserNotFound)
from soupsupply.soup_service.user import User, UserState


class SoupService:
    def __init__(self, user_repo: DynamodbUserRepository, soup_api: SoupAPI):
        self.user_repo = user_repo
        self.soup_api = soup_api

    def get_or_create_user(self, user_id: int) -> User:
        try:
            user = self.user_repo.get_user_by_user_id(user_id)
        except UserNotFound:
            user = User(
                user_id=user_id,
            )
            self.user_repo.save_user(user)

        return user

    def update_user_state(self, user_id: int, state: UserState) -> None:
        self.user_repo.update_item(user_id, data={
            'state': state,
        })

    def update_card_number(self, user_id: int, card: Optional[str]) -> None:
        self.user_repo.update_item(user_id, data={'card': card})

    def get_card_data(self, user_id: int) -> CardData:
        """
        Returns card data.

        :raises: UserCardIsMissing, SoupApiException,
            InvalidCardNumber, UnknownCard
        """
        user = self.user_repo.get_user_by_user_id(user_id)

        if user.card is None:
            raise UserCardIsMissing()

        return self.soup_api.get_data(user.card)

    def validate_card(self, card: str) -> str:
        stripped = str(card).replace(' ', '')

        if not stripped.isdigit():
            raise CardIsNotNumeric()

        if len(stripped) > 13:
            raise CardNumberIsTooLong()

        if len(stripped) < 13:
            raise CardNumberIsTooShort()

        self.soup_api.get_data(stripped)

        return stripped
