import pytest

from dynamodb import migrate
from soupsupply.containers import build_container
from soupsupply.lambdas import tg_controller_lambda


@pytest.fixture(scope='session')
def container():
    container = build_container()
    container.wire(modules=[migrate, tg_controller_lambda])
    yield container
    container.unwire()


@pytest.fixture(scope='session')
def dynamodb(container):
    migrate.create_users_tb()
    yield
    migrate.drop_users_tb()
