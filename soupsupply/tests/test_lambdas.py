from decimal import Decimal
from unittest.mock import Mock

from soupsupply.lambdas.get_card_info import get_card_info_lambda


def test_soup_lambda_without_history():
    # mock soup service
    mocked_soup = Mock()
    mocked_soup.get_card_data.return_value = Mock(
        history=[],
        balance=Mock(availableAmount=Decimal(100)),
    )

    # call target func
    data = get_card_info_lambda(
        event={'card': 'any'},
        context=None,
        soup_service=mocked_soup,
    )

    # assert response equality
    assert data == {
        'available_balance': Decimal(100),
        'last_purchase': None,
    }


def test_soup_lambda_with_history():
    # mock soup service
    mocked_soup = Mock()
    mocked_soup.get_card_data.return_value = Mock(
        history=[
            Mock(locationName='latest_place', amount=Decimal(15)),
            Mock(locationName='first_place', amount=Decimal(11)),
        ],
        balance=Mock(availableAmount=Decimal(100)),
    )

    # call target func
    data = get_card_info_lambda(
        event={'card': 'any'},
        context=None,
        soup_service=mocked_soup,
    )

    # assert response equality
    assert data == {
        'available_balance': Decimal(100),
        'last_purchase': {
            'place': 'latest_place',
            'amount': Decimal(15),
        },
    }
