from unittest import mock

import pytest

from soupsupply.soup_api import SoupAPI
from soupsupply.soup_api.data import CardData
from soupsupply.soup_api.exceptions import SoupApiException
from soupsupply.soup_service.exceptions import (CardIsNotNumeric,
                                                CardNumberIsTooLong,
                                                CardNumberIsTooShort,
                                                UserCardIsMissing)
from soupsupply.soup_service.service import SoupService
from soupsupply.soup_service.user import User


@pytest.fixture
def soup_service(dynamodb, container) -> SoupService:
    return container.soup_service()


def test_get_or_create_user(soup_service):
    # test creating new empty user
    user = soup_service.get_or_create_user(1)
    assert user == User(user_id=1)


def test_update_user_card(soup_service):
    user_id = 2
    user = soup_service.get_or_create_user(user_id)

    soup_service.update_card_number(user.user_id, '1234567890')
    user = soup_service.get_or_create_user(user.user_id)
    assert user.card == '1234567890'

    soup_service.update_card_number(user.user_id, '0987654321')
    user = soup_service.get_or_create_user(user.user_id)
    assert user.card == '0987654321'


def test_get_card_data(dynamodb, container):
    soup_api = mock.Mock(spec=SoupAPI)
    soup_api.get_data.return_value = CardData(**{
        'phone': '+1234567890',
        'balance': {'availableAmount': '5000'},
        'history': [],
        'smsInfoStatus': False,
    })

    with container.soup_api.override(soup_api):
        soup_service = container.soup_service()

        user_id = 3
        user = soup_service.get_or_create_user(user_id)
        soup_service.update_card_number(user.user_id, '1357924680')

        data = soup_service.get_card_data(user.user_id)
        assert isinstance(data, CardData)


def test_get_card_data_with_missing_card(soup_service):
    user_id = 4
    user = soup_service.get_or_create_user(user_id)

    # new user has no card
    with pytest.raises(UserCardIsMissing):
        soup_service.get_card_data(user.user_id)


def test_validate_card(container):
    # test positive with strip
    soup_api = mock.Mock(spec=SoupAPI)
    soup_api.get_data.return_value = CardData(**{
        'phone': '+1234567890',
        'balance': {'availableAmount': '5000'},
        'history': [],
        'smsInfoStatus': False,
    })

    with container.soup_api.override(soup_api):
        soup_service = container.soup_service()
        card = soup_service.validate_card(' 1234 5678 9012 3 ')
        assert card == '1234567890123'


def test_validate_non_existent_card(container):
    # test non existent card number
    broken_soup_api = mock.Mock(spec=SoupAPI)
    broken_soup_api.get_data.side_effect = SoupApiException()

    with container.soup_api.override(broken_soup_api):
        soup_service = container.soup_service()
        with pytest.raises(SoupApiException):
            soup_service.validate_card(' 1234 5678 9012 3 ')


def test_validate_card_structural(soup_service):
    # test too long invalid card number
    with pytest.raises(CardNumberIsTooLong):
        soup_service.validate_card('123432435436547567')

    # test too short invalid card number
    with pytest.raises(CardNumberIsTooShort):
        soup_service.validate_card('123')

    # test not numeric short invalid card number
    with pytest.raises(CardIsNotNumeric):
        soup_service.validate_card('a132')

    # test non numeric card number
    with pytest.raises(CardIsNotNumeric):
        soup_service.validate_card('123456789012s')
