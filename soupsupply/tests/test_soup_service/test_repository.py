import random

import pytest

from soupsupply.repository.dynamodb import DynamodbUserRepository
from soupsupply.soup_service.exceptions import UserNotFound
from soupsupply.soup_service.user import User, UserState


@pytest.fixture
def repository(dynamodb, container) -> DynamodbUserRepository:
    return container.user_repository()


def get_user_id() -> int:
    return random.randint(10**5, 10**6)


def test_insert_user(repository: DynamodbUserRepository):
    user_id = get_user_id()

    created_user = User(
        user_id=user_id,
        card='1234567890',
        is_monitored=True,
        is_superuser=True,
    )
    repository.save_user(created_user)

    fetched_user = repository.get_user_by_user_id(user_id)

    assert created_user == fetched_user


def test_user_not_found(repository: DynamodbUserRepository):
    user_id = get_user_id()

    with pytest.raises(UserNotFound):
        repository.get_user_by_user_id(user_id)


def test_update_user(repository: DynamodbUserRepository):
    user_id = get_user_id()

    created_user = User(
        user_id=user_id,
        card='1234567890',
    )

    repository.save_user(created_user)

    repository.update_item(user_id, {'card': '0987654321'})

    fetched_user = repository.get_user_by_user_id(user_id)

    assert fetched_user.card == '0987654321'


def test_delete_user_card(repository: DynamodbUserRepository):
    user_id = get_user_id()

    created_user = User(
        user_id=user_id,
        card='1234567890',
    )

    repository.save_user(created_user)

    repository.update_item(user_id, {'card': None})

    fetched_user = repository.get_user_by_user_id(user_id)

    assert fetched_user.card is None


def test_delete_user(repository: DynamodbUserRepository):
    user_id = get_user_id()

    created_user = User(
        user_id=user_id,
    )

    repository.save_user(created_user)

    repository.delete_item(user_id)

    with pytest.raises(UserNotFound):
        repository.get_user_by_user_id(user_id)


def test_update_user_state(repository):
    user_id = get_user_id()

    user = User(
        user_id=user_id,
    )

    repository.save_user(user)

    repository.update_item(user.user_id, data={
        'state': UserState.ON_CARD_INPUT,
    })

    fetched_user = repository.get_user_by_user_id(user.user_id)
    assert fetched_user.state == UserState.ON_CARD_INPUT
