import json
import os
from decimal import Decimal
from unittest.mock import Mock

import pytest
import requests

from soupsupply.soup_api import SoupAPI
from soupsupply.soup_api.exceptions import InvalidCardNumber, UnknownCard


def test_request_serialization(monkeypatch):
    # get mocked response data
    base_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(base_path, 'real_response.json')) as f:
        data = f.read()

    # mock requests
    mocked_response = Mock()
    mocked_response.json.return_value = json.loads(data)
    mocked_get_func = Mock(return_value=mocked_response)

    monkeypatch.setattr(requests, 'get', mocked_get_func)

    # ask for info
    card_data = SoupAPI.get_data('1234567')

    # assert basic data equality
    assert card_data.phone == '+7 (123) - *** - ** -45'
    assert card_data.balance.availableAmount == Decimal('4730')
    assert len(card_data.history) == 14


def test_unknown_card(monkeypatch):
    mocked_response = Mock()
    mocked_response.json.return_value = {
        'status': 'BAD_REQUEST',
        'messages': [
            {
                'type': 'ERROR',
                'context': 'LOCAL',
                'code': 'BAD_REQUEST'
            }
        ]
    }
    mocked_get_func = Mock(return_value=mocked_response)

    monkeypatch.setattr(requests, 'get', mocked_get_func)

    # ask for info
    with pytest.raises(UnknownCard):
        SoupAPI.get_data('1234567')


def test_invalid_card(monkeypatch):
    mocked_response = Mock()
    mocked_response.json.return_value = {
        'status': 'VALIDATION_FAIL',
        'messages': [
            {
                'path': 'ean',
                'type': 'ERROR',
                'context': 'FIELD',
                'code': 'VALIDATION_FAIL'
            }
        ]
    }
    mocked_get_func = Mock(return_value=mocked_response)

    monkeypatch.setattr(requests, 'get', mocked_get_func)

    # ask for info
    with pytest.raises(InvalidCardNumber):
        SoupAPI.get_data('1234567')
