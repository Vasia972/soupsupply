from unittest import mock

from soupsupply.containers import Container
from soupsupply.lambdas.tg_controller_lambda import _tg_controller_lambda
from soupsupply.soup_service.service import SoupService
from soupsupply.tests.test_tg_service.aws_event import event
from soupsupply.tg.api import TgAPI


def test_process_tg_webhook(container: Container):
    tg_api_mock = mock.Mock(spec=TgAPI)
    soup_service_mock = mock.Mock(spec=SoupService)

    with container.tg_api.override(tg_api_mock),\
            container.soup_service.override(soup_service_mock):
        _tg_controller_lambda(event=event, context=object())

    chat_id, msg, keyboard = tg_api_mock.send_message.call_args[0]

    assert chat_id == 123456789
    assert msg

    user_id, = soup_service_mock.get_or_create_user.call_args[0]
    assert user_id == 987654321
