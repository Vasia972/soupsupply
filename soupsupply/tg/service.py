import enum
import random
from typing import Optional, Union

from soupsupply.soup_service.service import SoupService

from ..soup_api.exceptions import (InvalidCardNumber, SoupApiException,
                                   UnknownCard)
from ..soup_service.exceptions import (CardIsNotNumeric, CardNumberIsTooLong,
                                       CardNumberIsTooShort,
                                       CardValidationError, UserCardIsMissing)
from ..soup_service.user import User, UserState
from .api import TgAPI
from .dtos import (KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove,
                   TgUpdate)


class Commands(enum.Enum):
    SAVE_CARD = 'сохранить карту'
    CHANGE_CARD = 'поменять карту'
    GET_BALANCE = 'баланс'


class TgSoupService:
    def __init__(self, tg_api: TgAPI, soup_service: SoupService):
        self.tg_api = tg_api
        self.soup_service = soup_service

    def process_message(self, update: TgUpdate):
        message = update.message

        user_id = message.from_.id
        chat_id = message.chat.id
        text = message.text

        user = self.soup_service.get_or_create_user(user_id)

        if text == '/start':
            self._start(user, chat_id)
        elif text == Commands.CHANGE_CARD.value:
            self._set_card(user, chat_id)
        elif text == Commands.GET_BALANCE.value:
            self._balance(user, chat_id)
        elif user.state == UserState.ON_CARD_INPUT:
            self._on_card_input(user, chat_id, text)
        else:
            self._unknown(chat_id, user)

    def _start(self, user: User, chat_id: int):
        self.send_message(chat_id, user, random.choice([
            'здарова заебал', 'ку епт', 'салем берды',
        ]))

        if user.state == UserState.NEW:
            self._set_card(user, chat_id)

    def _set_card(self, user: User, chat_id: int) -> None:
        user.state = UserState.ON_CARD_INPUT
        self.soup_service.update_user_state(user.user_id,
                                            UserState.ON_CARD_INPUT)
        self.send_message(chat_id, user, 'диктуй цифры')

    def _on_card_input(self, user: User, chat_id: int, text: str) -> None:
        message = None
        try:
            card = self.soup_service.validate_card(text)

            self.soup_service.update_card_number(user.user_id, card)

            user.state = UserState.READY
            self.soup_service.update_user_state(user.user_id, UserState.READY)

            message = 'принято'
        except CardNumberIsTooShort:
            message = 'слишком коротко'
        except CardNumberIsTooLong:
            message = 'слишком длинно'
        except CardIsNotNumeric:
            message = 'букв не может быть в номере дядь'
        except (CardValidationError, InvalidCardNumber):
            message = 'какая то не такая у тебя карта'
        except UnknownCard:
            message = 'фальшивка, не принимается'
        except SoupApiException:
            message = 'какая то хуйня, не могу чекнуть. попозже давай'
        finally:
            if message is not None:
                self.send_message(chat_id, user, message)

    def _balance(self, user: User, chat_id: int):
        message = None
        try:
            data = self.soup_service.get_card_data(user.user_id)
            message_start = random.choice(
                ['имеется', 'на балике', 'есть', 'там']
            )
            message = f'{message_start} {data.balance.availableAmount} рублей'  # noqa:E501
        except UserCardIsMissing:
            message = 'сначала карту вбей'
            user.state = UserState.ON_CARD_INPUT
            self.soup_service.update_user_state(user.user_id,
                                                UserState.ON_CARD_INPUT)
        except SoupApiException:
            message = 'эта хуйня лежит, не прочекать'
        except (InvalidCardNumber, UnknownCard):
            message = 'у тя карта невалидная. давай по новой'
            self.soup_service.update_card_number(user.user_id, None)
            user.state = UserState.ON_CARD_INPUT
            self.soup_service.update_user_state(user.user_id,
                                                UserState.ON_CARD_INPUT)
        finally:
            if message is not None:
                self.send_message(chat_id, user, message)

    def _unknown(self, chat_id: int, user: User):
        message = random.choice([
            'а?', 'кто кто?', 'что что?', 'кавооо'
        ])
        self.send_message(chat_id, user, message)

    def send_message(self, chat_id, user, message):
        keyboard = self._make_keyboard(user)
        self.tg_api.send_message(chat_id, message, keyboard)

    @staticmethod
    def _make_keyboard(user: User) -> Optional[
        Union[ReplyKeyboardMarkup, ReplyKeyboardRemove]
    ]:
        if user.state == UserState.NEW:
            return ReplyKeyboardMarkup(
                keyboard=[
                    [KeyboardButton(text=Commands.SAVE_CARD.value)],
                ],
                resize_keyboard=True,
            )

        if user.state == UserState.ON_CARD_INPUT:
            return ReplyKeyboardRemove()

        if user.state == UserState.READY:
            return ReplyKeyboardMarkup(
                keyboard=[
                    [KeyboardButton(text=Commands.GET_BALANCE.value)],
                    [KeyboardButton(text=Commands.CHANGE_CARD.value)],
                ],
                resize_keyboard=True,
            )

        return None
