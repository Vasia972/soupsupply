from typing import Any, Optional, Union

import requests

from soupsupply.tg.dtos import ReplyKeyboardMarkup, ReplyKeyboardRemove


class TgAPI:
    def __init__(self, bot_token: str):
        self.bot_token = bot_token

    def send_message(self,
                     chat_id: int,
                     text: str,
                     keyboard: Optional[Union[
                         ReplyKeyboardMarkup,
                         ReplyKeyboardRemove
                     ]] = None) -> None:
        data = {
            'chat_id': chat_id,
            'text': text,
        }

        if keyboard is not None:
            data['reply_markup'] = keyboard.dict(exclude_none=True)

        resp = self.call('post', 'sendMessage', json=data)

        resp.raise_for_status()

    def call(self,
             http_method: str,
             tg_method: str,
             json: Optional[dict[Any]] = None) -> requests.Response:
        return requests.request(
            method=http_method,
            url=f'https://api.telegram.org/bot{self.bot_token}/{tg_method}',
            json=json,
        )
