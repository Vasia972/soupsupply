import datetime
from typing import Optional

from pydantic import BaseModel, Field


class TgChat(BaseModel):
    id: int
    type: str
    title: Optional[str]
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]


class TgUser(BaseModel):
    id: int
    is_bot: bool
    first_name: str
    last_name: Optional[str]
    username: Optional[str]


class TgMessage(BaseModel):
    message_id: int
    text: str
    from_: Optional[TgUser]
    sender_chat: Optional[TgChat]
    date: datetime.datetime
    chat: TgChat
    forward_from: Optional[TgUser]
    forward_from_chat: Optional[TgChat]

    class Config:
        fields = {
            'from_': 'from'
        }


class TgUpdate(BaseModel):
    update_id: int
    message: TgMessage


# ############ Telegram objects for interaction #############

class KeyboardButtonPollType(BaseModel):
    type: str


class KeyboardButton(BaseModel):
    text: str
    request_contact: Optional[bool]
    request_location: Optional[bool]
    request_poll: Optional[KeyboardButtonPollType]


class ReplyKeyboardMarkup(BaseModel):
    keyboard: list[list[KeyboardButton]]
    resize_keyboard: Optional[bool]
    one_time_keyboard: Optional[bool]
    input_field_placeholder: Optional[str]
    selective: Optional[bool]


class ReplyKeyboardRemove(BaseModel):
    remove_keyboard: bool = Field(True, const=True)
    selective: Optional[bool]
