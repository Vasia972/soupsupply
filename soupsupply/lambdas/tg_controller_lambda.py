import sys
import traceback

from dependency_injector.wiring import Provide, inject

from soupsupply.containers import Container, build_container
from soupsupply.tg.dtos import TgUpdate
from soupsupply.tg.service import TgSoupService


@inject
def _tg_controller_lambda(event: dict,
                          context: object,
                          service: TgSoupService = Provide[
                              Container.tg_service
                          ]):
    body: str = event['body']
    service.process_message(TgUpdate.parse_raw(body))


def tg_controller_lambda(event: dict, context: object):
    try:
        print('Event', event)
        print('Context', context)

        container = build_container()
        container.wire(container.wire(modules=[sys.modules[__name__]]))

        _tg_controller_lambda(event=event, context=context)
    except Exception as e:
        print('Error', str(e), traceback.format_exc())

    return {
        'statusCode': 200,
        'body': 'ok'
    }
