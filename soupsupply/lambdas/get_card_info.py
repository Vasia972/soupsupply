from dependency_injector.wiring import Provide, inject

from soupsupply.containers import Container
from soupsupply.soup_service.service import SoupService


@inject
def get_card_info_lambda(
        event,
        context,
        soup_service: SoupService = Provide[Container.soup_service],
):
    card_data = soup_service.get_card_data(event['card'])
    last_purchase = card_data.history and {
            'place': card_data.history[0].locationName,
            'amount': card_data.history[0].amount,
    } or None

    return {
        'available_balance': card_data.balance.availableAmount,
        'last_purchase': last_purchase,
    }
