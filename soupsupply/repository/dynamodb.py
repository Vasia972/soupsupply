import enum
from typing import Any, Union

from soupsupply.soup_service.exceptions import UserNotFound
from soupsupply.soup_service.user import User


class DynamodbUserRepository:
    def __init__(self, dynamodb: Any):
        self.dynamodb = dynamodb
        self.table = dynamodb.Table('SoupUsers')

    def save_user(self, user: User) -> None:
        self.table.put_item(
            Item=self._to_db(user),
        )

    def get_user_by_user_id(self, user_id: int) -> User:
        response = self.table.get_item(
            Key={
                'user_id':  user_id,
            },
        )
        if 'Item' not in response:
            raise UserNotFound(f'User not found! {user_id=}')
        user = response['Item']
        return User(**user)

    def update_item(self, user_id: int, data: dict[str, Any]) -> None:
        converted_data = self._to_db(data)
        updates = {
            key:
                {'Action': 'PUT' if value is not None else 'DELETE'}
                | ({'Value': value} if value is not None else {})
            for key, value in converted_data.items()
        }

        self.table.update_item(
            Key={
                'user_id': user_id,
            },
            AttributeUpdates=updates,
        )

    def delete_item(self, user_id: int) -> None:
        self.table.delete_item(
            Key={
                'user_id': user_id
            },
        )

    @staticmethod
    def _to_db(
            user: Union[dict[str, Any], User]
    ) -> dict[str, Union[str, int]]:
        def convert(item) -> tuple[str, Union[str, int]]:
            """Convert enums to str."""
            key, value = item
            if isinstance(value, enum.Enum):
                return key, value.value
            return key, value

        if isinstance(user, dict):
            user_dict = user
        elif isinstance(user, User):
            user_dict = user.dict()
        else:
            raise TypeError('`user` should be type of <dict> or <User>')

        return dict(map(convert, user_dict.items()))
