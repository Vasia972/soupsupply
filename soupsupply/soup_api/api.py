import urllib.parse

import requests

from .data import CardData, CardRequestData
from .exceptions import InvalidCardNumber, SoupApiException, UnknownCard


class SoupAPI:

    API_URL = 'https://meal.gift-cards.ru/api/1/cards/'

    @classmethod
    def get_data(cls, card: str) -> CardData:
        response = requests.get(urllib.parse.urljoin(cls.API_URL, card))

        if not response.ok:
            msg = f'Unrecognized error. Error: {response.text}'
            raise SoupApiException(msg)

        data = response.json()
        status = data['status']

        if status == 'OK':
            return CardRequestData(**data).data
        elif status == 'VALIDATION_FAIL':
            raise InvalidCardNumber()
        elif status == 'BAD_REQUEST':
            raise UnknownCard()
        else:
            raise SoupApiException('Unknown status code')
