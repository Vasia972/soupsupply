from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel


class History(BaseModel):
    time: datetime
    amount: Decimal
    locationName: list[str]
    currency: str
    merchantId: str
    reversal: bool


class Balance(BaseModel):
    availableAmount: Decimal


class CardData(BaseModel):
    phone: str
    balance: Balance
    history: list[History]
    smsInfoStatus: str


class CardRequestData(BaseModel):
    status: str
    data: CardData
