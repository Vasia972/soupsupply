class SoupApiException(Exception):
    pass


class InvalidCardNumber(SoupApiException):
    """Card number is semantically incorrect."""


class UnknownCard(SoupApiException):
    """Card does not exist."""
